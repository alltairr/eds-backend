package br.com.altair.eds.repository;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.altair.eds.models.Grupo;
import br.com.altair.eds.repository.GrupoRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class GrupoRepositoryTest {

	@Autowired
    private TestEntityManager entityManager;
 
    @Autowired
    private GrupoRepository grupoRepository;
    
    @Test
    public void whenFinById() {
    	Grupo grupo = new Grupo();
    	grupo.setNome("GRUPO 1");
    	
    	grupo = entityManager.persist(grupo);
    	entityManager.flush();
    	
    	Grupo found = grupoRepository.findById(grupo.getId()).get();
    	
    	assertEquals(found.getNome(), grupo.getNome());
    }
}
