package br.com.altair.eds.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import br.com.altair.eds.models.Usuario;
import br.com.altair.eds.repository.UsuarioRepository;

@Service
public class UsuarioServiceImpl extends BaseServiceImpl<Usuario, Long> implements UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Override
	public Usuario atualiza(Long id, Usuario usuario) {
		return usuarioRepository.findById(id).map(usuarioAtual -> {
			usuarioAtual.setNome(usuario.getNome());
			usuarioAtual.setSobreNome(usuario.getSobreNome());
			usuarioAtual.setEmail(usuario.getEmail());
			usuarioAtual.setGrupo(usuario.getGrupo());
			return usuarioRepository.save(usuarioAtual);
		}).orElseThrow(() -> new RuntimeException("Grupo não encontrado"));
	}

	@Override
	public JpaRepository<Usuario, Long> getRepository() {
		return usuarioRepository;
	}

}
