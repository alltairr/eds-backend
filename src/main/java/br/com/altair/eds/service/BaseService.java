package br.com.altair.eds.service;

import java.util.List;

public interface BaseService<T, ID> {

	T buscaPorId(ID id);
	
	List<T> listarTodos();
	
	T salva(T t);
	
	T atualiza(ID id, T t);
	
	void remove(ID id);
}
