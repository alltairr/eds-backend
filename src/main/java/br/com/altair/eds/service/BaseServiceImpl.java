package br.com.altair.eds.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public abstract class BaseServiceImpl<T, ID> {

	public abstract JpaRepository<T, ID> getRepository();

	public T buscaPorId(ID id) {
		return getRepository().findById(id).get();
	}

	public List<T> listarTodos() {
		return getRepository().findAll();
	}

	public T salva(T t) {
		return getRepository().save(t);
	}

	public void remove(ID id) {
		getRepository().findById(id).map(entity -> {
			getRepository().delete(entity);
			return entity;
		}).orElseThrow(() -> new RuntimeException("Grupo não encontrado"));
	}

}
