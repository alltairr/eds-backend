package br.com.altair.eds.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import br.com.altair.eds.models.Grupo;
import br.com.altair.eds.repository.GrupoRepository;

@Service
public class GrupoServiceImpl extends BaseServiceImpl<Grupo, Long> implements GrupoService {

	@Autowired
	private GrupoRepository grupoRepository;
	
	@Override
	public Grupo salva(Grupo grupo) {
		grupo.getUsuarios().forEach(usuario -> usuario.setGrupo(grupo));
		return super.salva(grupo);
	}
	
	@Override
	public Grupo atualiza(Long idGrupo, Grupo grupo) {
		return grupoRepository.findById(idGrupo).map(grupoAtual -> {
			grupoAtual.setNome(grupo.getNome());
			grupoAtual.setUsuarios(grupo.getUsuarios());
			return grupoRepository.save(grupoAtual);
		}).orElseThrow(() -> new RuntimeException("Grupo não encontrado"));
	}

	@Override
	public JpaRepository<Grupo, Long> getRepository() {
		return grupoRepository;
	}

}
