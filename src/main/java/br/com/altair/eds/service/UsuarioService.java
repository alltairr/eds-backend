package br.com.altair.eds.service;

import br.com.altair.eds.models.Usuario;

public interface UsuarioService extends BaseService<Usuario, Long>  {
	
}
