package br.com.altair.eds.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.altair.eds.models.Usuario;
import br.com.altair.eds.service.UsuarioService;

@RestController
@RequestMapping(UsuarioController.BASE_URL)
public class UsuarioController {
		
	public final static String BASE_URL = "/usuarios";
	
	@Autowired
	private UsuarioService usuarioService;
	
	@GetMapping
	public @ResponseBody List<Usuario> listarTodos(){
		return usuarioService.listarTodos();
	}
	
	@PostMapping
	public Usuario salva(@Valid @RequestBody Usuario usuario) {
		return usuarioService.salva(usuario);
	}

	@PutMapping("/{idUsuario}")
	public Usuario atualiza(@PathVariable Long idUsuario, @Valid @RequestBody Usuario usuario) {
		return usuarioService.atualiza(idUsuario, usuario);
	}
	
	@DeleteMapping("/{idUsuario}")
    public ResponseEntity<?> remove(@PathVariable Long idUsuario) {
		usuarioService.remove(idUsuario);
        return ResponseEntity.ok().build();
    }

	
}
