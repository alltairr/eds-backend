package br.com.altair.eds.resource;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.altair.eds.models.Grupo;
import br.com.altair.eds.models.Usuario;
import br.com.altair.eds.service.GrupoService;

@RestController
@RequestMapping(GrupoController.BASE_URL)
public class GrupoController {

	public final static String BASE_URL = "/grupos";

	@Autowired
	private GrupoService grupoService;

	@GetMapping
	public List<Grupo> listarTodos() {
		return grupoService.listarTodos();
	}
	
	@GetMapping("/{idGrupo}/usuarios")
	public List<Usuario> listarTodosUsuarios(@PathVariable Long idGrupo) {
		return grupoService.buscaPorId(idGrupo).getUsuarios();
	}

	@PostMapping
	public Grupo salva(@Valid @RequestBody Grupo grupo) {
		return grupoService.salva(grupo);
	}

	@PutMapping("/{idGrupo}")
	public Grupo atualiza(@PathVariable Long idGrupo, @Valid @RequestBody Grupo grupo) {
		return grupoService.atualiza(idGrupo, grupo);
	}
	
	@DeleteMapping("/{idGrupo}")
    public ResponseEntity<?> remove(@PathVariable Long idGrupo) {
        grupoService.remove(idGrupo);
        return ResponseEntity.ok().build();
    }
}
