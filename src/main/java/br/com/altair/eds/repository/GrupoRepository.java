package br.com.altair.eds.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.altair.eds.models.Grupo;

@Repository
public interface GrupoRepository extends JpaRepository<Grupo, Long> {

}
