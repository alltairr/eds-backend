package br.com.altair.eds.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.altair.eds.models.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long>{
}
